import { classNames } from "shared/lib/classNames/classNames";
import { memo } from "react";
import { useSelector } from "react-redux";
import { getUserId } from "entities/User";

import { CalendarConrolPanel } from "features/CalendarControlPanel";
import { DateModal } from "entities/Date";
import { Calendar } from "widgets/Calendar";
import s from "./UserCalendarPage.module.scss";

interface UserCalendarPageProps {
  className?: string;
}

const UserCalendarPage = ({ className }: UserCalendarPageProps) => {
  const userId = useSelector(getUserId);

  return (
    <div className={classNames(s.CalendarPage, {}, [className])}>
      <CalendarConrolPanel />
      <Calendar userId={userId} />
      <DateModal />
    </div>
  );
};

export default memo(UserCalendarPage);
