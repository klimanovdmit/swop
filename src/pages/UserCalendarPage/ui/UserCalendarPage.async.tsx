import { lazy } from "react";

export const UserCalendarPageAsync = lazy(
  () =>
    new Promise((resolve) => {
      // @ts-ignore
      setTimeout(() => resolve(import("./UserCalendarPage")), 1500);
    })
);
