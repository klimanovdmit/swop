import { Button } from "antd";
import { classNames } from "shared/lib/classNames/classNames";
import { useSelector } from "react-redux";
import { getProfileReadonly, ProfileActions } from "entities/Profile";
import { useCallback } from "react";
import s from "./ProfilePageHeader.module.scss";
import { useAppDispatch } from "../../../../shared/lib/hooks/UseAppDispatch/useAppDispatch";

interface ProfilePageHeaderProps {
  className?: string;
}

export const ProfilePageHeader = ({ className }: ProfilePageHeaderProps) => {
  const dispatch = useAppDispatch();
  const readonly = useSelector(getProfileReadonly);

  const onEdit = useCallback(() => {
    dispatch(ProfileActions.setReadonly(false));
  }, []);

  const onCancelEdit = useCallback(() => {
    dispatch(ProfileActions.cancelEdit());
  }, []);

  return (
    <div className={classNames(s.ProfilePageHeader, {}, [className])}>
      <div className={s.header}>
        <h1>Профиль</h1>
        {readonly ? (
          <Button type="primary" onClick={onEdit}>
            Редактировать
          </Button>
        ) : (
          <Button type="default" onClick={onCancelEdit}>
            Отменить
          </Button>
        )}
      </div>
    </div>
  );
};
