import {
  fetchProfileData,
  getProfileError,
  getProfileForm,
  getProfileIsLoading,
  getProfileReadonly,
  Profile,
  ProfileCard,
} from "entities/Profile";
import {
  ProfileActions,
  ProfileReducer,
} from "entities/Profile/model/slice/ProfileSlice";
import { useCallback, useEffect } from "react";
import { useSelector } from "react-redux";
import { classNames } from "shared/lib/classNames/classNames";
import {
  DynamicMiduleLoader,
  ReducersList,
} from "shared/lib/components/DynamicMiduleLoader/DynamicMiduleLoader";
import { useAppDispatch } from "shared/lib/hooks/UseAppDispatch/useAppDispatch";
import s from "./ProfilePage.module.scss";
import { ProfilePageHeader } from "./ProfilePageHeader/ProfilePageHeader";

interface ProfilePageProps {
  className?: string;
}

const reducers: ReducersList = {
  profile: ProfileReducer,
};

export const ProfilePage = ({ className }: ProfilePageProps) => {
  const dispatch = useAppDispatch();
  const formData = useSelector(getProfileForm);
  const isLoading = useSelector(getProfileIsLoading);
  const error = useSelector(getProfileError);
  const readonly = useSelector(getProfileReadonly);

  const onChangeProfileData = useCallback(
    (data?: Profile) => dispatch(ProfileActions.updateProfile(data || {})),
    [dispatch]
  );

  useEffect(() => {
    dispatch(fetchProfileData());
  }, [dispatch]);
  return (
    <DynamicMiduleLoader reducers={reducers} removeAfterUnmount>
      <div className={classNames(s.ProfilePage, {}, [className])}>
        <ProfilePageHeader />
        <ProfileCard
          data={formData}
          isLoading={isLoading}
          error={error}
          onChangeProfileData={onChangeProfileData}
          readonly={readonly}
        />
      </div>
    </DynamicMiduleLoader>
  );
};

export default ProfilePage;
