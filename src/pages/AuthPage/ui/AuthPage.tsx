import { classNames } from "shared/lib/classNames/classNames";
import s from "./AuthPage.module.scss";

interface AuthPageProps {
  className?: string;
}
const AuthPage = ({ className }: AuthPageProps) => (
  <div className={classNames(s.AuthPage, {}, [className])}>AuthPage</div>
);

export default AuthPage;
