import { classNames } from "shared/lib/classNames/classNames";
import { memo } from "react";

import { useParams } from "react-router-dom";
import { Calendar } from "widgets/Calendar/ui/Calendar";
import s from "./CalendarPage.module.scss";

interface CalendarPageProps {
  className?: string;
}

const CalendarPage = ({ className }: CalendarPageProps) => {
  const { id } = useParams<{ id: string }>();

  if (!id) {
    return (
      <div className={classNames(s.CalendarPage, {}, [className])}>
        Календарь не найден
      </div>
    );
  }

  return (
    <div className={classNames(s.CalendarPage, {}, [className])}>
      <Calendar userId={id} />
    </div>
  );
};

export default memo(CalendarPage);
