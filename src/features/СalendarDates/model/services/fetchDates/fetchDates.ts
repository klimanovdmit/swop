import { createAsyncThunk } from "@reduxjs/toolkit";
import { ThunkConfig } from "app/providers/StoreProvider/config/StateShema";
import { getUserId } from "entities/User";
import { Date } from "entities/Date";

export const fetchDates = createAsyncThunk<Date[], string, ThunkConfig<string>>(
  "calendarDates/fetchDates",

  async (userId, thunkApi) => {
    const { extra, rejectWithValue, getState } = thunkApi;

    try {
      const response = await extra.api.get<Date[]>(`/dates`, {
        params: { userId, _expand: "user" },
      });

      return response.data;
    } catch (e) {
      return rejectWithValue("error");
    }
  }
);
