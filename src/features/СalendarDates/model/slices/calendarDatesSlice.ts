import {
  createEntityAdapter,
  createSlice,
  PayloadAction,
} from "@reduxjs/toolkit";
import { Date } from "entities/Date";
import { StateSchema } from "../../../../app/providers/StoreProvider/config/StateShema";
import { fetchDates } from "../services/fetchDates/fetchDates";
import { CalendarDatesSchema } from "../types/CalendarDatesSchema";

const datesAdapter = createEntityAdapter<Date>({
  selectId: (date) => date.id,
});

export const getCalendarDates = datesAdapter.getSelectors<StateSchema>(
  (state) => state.calendarDates || datesAdapter.getInitialState()
);

const calendarDatesSlice = createSlice({
  name: "calendarDatesSlice",
  initialState: datesAdapter.getInitialState<CalendarDatesSchema>({
    isLoading: false,
    error: undefined,
    ids: [],
    entities: {},
  }),
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchDates.pending, (state) => {
        state.error = undefined;
        state.isLoading = true;
      })

      .addCase(fetchDates.fulfilled, (state, action: PayloadAction<Date[]>) => {
        state.isLoading = false;
        datesAdapter.setAll(state, action.payload);
      })

      .addCase(fetchDates.rejected, (state, action) => {
        state.isLoading = false;
        state.error = action.payload;
      });
  },
});

export const { reducer: calendarDatesReducer } = calendarDatesSlice;
