import { EntityState } from "@reduxjs/toolkit";
import { Date } from "entities/Date";

export interface CalendarDatesSchema extends EntityState<Date> {
  isLoading: boolean;
  error?: string;
}
