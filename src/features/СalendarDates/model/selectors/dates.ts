import { StateSchema } from "app/providers/StoreProvider/config/StateShema";

export const getCalendarDatesIsLoading = (state: StateSchema) =>
  state.calendarDates.isLoading;
export const getCalendarDatesError = (state: StateSchema) =>
  state.calendarDates.isLoading;
