import {
  DeleteOutlined,
  FieldTimeOutlined,
  UserAddOutlined,
} from "@ant-design/icons";
import { Button } from "antd";
import { classNames } from "shared/lib/classNames/classNames";
import { useSelector } from "react-redux";
import {
  DynamicMiduleLoader,
  ReducersList,
} from "shared/lib/components/DynamicMiduleLoader/DynamicMiduleLoader";
import s from "./CalendarConrolPanel.module.scss";
import { useAppDispatch } from "../../../shared/lib/hooks/UseAppDispatch/useAppDispatch";
import {
  calendarControlPanelActions,
  calendarControlPanelReducer,
} from "../model/slices/calendarControlPanelSlice";
import { Mode } from "../model/types/CalendarDatesSchema";
import { getCalendarControlPanelMode } from "../model/selectors/getCalendarControlPanelState/getCalendarControlPanelMode/getCalendarControlPanelMode";

interface CalendarControlPanelProps {
  className?: string;
}

const reducers: ReducersList = {
  calendarControlPanel: calendarControlPanelReducer,
};

export const CalendarControlPanel = ({
  className,
}: CalendarControlPanelProps) => {
  const dispatch = useAppDispatch();
  const mode = useSelector(getCalendarControlPanelMode);

  const setAddDateMode = () => {
    dispatch(calendarControlPanelActions.setMode(Mode.ADD_DATE));
  };

  const setDeleteDateMode = () => {
    dispatch(calendarControlPanelActions.setMode(Mode.DELETE_DATE));
  };
  return (
    <DynamicMiduleLoader reducers={reducers}>
      <div className={classNames(s.CalendarConrolPanel, {}, [className])}>
        {/* <Button type="primary" icon={<UserAddOutlined />} size="large" /> */}
        <Button
          type="primary"
          icon={<FieldTimeOutlined />}
          size="large"
          onClick={setAddDateMode}
          disabled={mode === Mode.ADD_DATE}
        />
        <Button
          type="default"
          icon={<DeleteOutlined />}
          size="large"
          onClick={setDeleteDateMode}
          disabled={mode === Mode.DELETE_DATE}
        />
      </div>
    </DynamicMiduleLoader>
  );
};
