import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { CalendarControlPanelSchema, Mode } from "../types/CalendarDatesSchema";

const initialState: CalendarControlPanelSchema = {
  mode: Mode.NONE,
  isLoading: false,
};

export const calendarControlPanelSlice = createSlice({
  name: "calendarControlPanel",
  initialState,
  reducers: {
    setMode: (state, action: PayloadAction<Mode>) => {
      state.mode = action.payload;
    },
  },

  // extraReducers: (builder) => {
  //   builder
  //     .addCase(loginByUserName.pending, (state) => {
  //       state.error = undefined;
  //       state.isLoading = true;
  //     })

  //     .addCase(loginByUserName.fulfilled, (state) => {
  //       state.isLoading = false;
  //     })

  //     .addCase(loginByUserName.rejected, (state, action) => {
  //       state.isLoading = false;
  //       state.error = action.payload;
  //     });
  // },
});

export const { actions: calendarControlPanelActions } =
  calendarControlPanelSlice;
export const { reducer: calendarControlPanelReducer } =
  calendarControlPanelSlice;
