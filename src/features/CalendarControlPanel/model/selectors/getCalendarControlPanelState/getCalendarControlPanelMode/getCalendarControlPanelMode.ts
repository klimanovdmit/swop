import { StateSchema } from "app/providers/StoreProvider";

export const getCalendarControlPanelMode = (state: StateSchema) =>
  state?.calendarControlPanel?.mode;
