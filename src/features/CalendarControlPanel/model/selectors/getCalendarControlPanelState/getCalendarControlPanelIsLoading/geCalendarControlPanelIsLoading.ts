import { StateSchema } from "app/providers/StoreProvider";

export const getCalendarControlPanelIsLoading = (state: StateSchema) =>
  state?.calendarControlPanel?.isLoading || false;
