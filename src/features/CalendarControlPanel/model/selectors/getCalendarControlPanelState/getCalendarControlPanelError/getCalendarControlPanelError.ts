import { StateSchema } from "app/providers/StoreProvider";

export const getCalendarControlPanelError = (state: StateSchema) =>
  state?.calendarControlPanel?.error;
