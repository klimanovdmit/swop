import { StateSchema } from "app/providers/StoreProvider";

export const getCalendarControlPanelState = (state: StateSchema) =>
  state?.calendarControlPanel;
