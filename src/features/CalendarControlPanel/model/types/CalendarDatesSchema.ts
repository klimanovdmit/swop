export enum Mode {
  ADD_DATE = "addDate",
  DELETE_DATE = "deleteDate",
  UPDATE_DATE = "updateDate",
  NONE = "none",
}

export interface CalendarControlPanelSchema {
  mode: Mode;
  isLoading: boolean;
  error?: string;
}
