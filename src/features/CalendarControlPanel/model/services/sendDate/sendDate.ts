import { createAsyncThunk } from "@reduxjs/toolkit";
import { ThunkConfig } from "app/providers/StoreProvider/config/StateShema";
import { Date, getDateData } from "entities/Date";
import { getUserId } from "entities/User/model/selectors/getUserId/getUserId";
import { getCalendarDates } from "features/СalendarDates/model/slices/calendarDatesSlice";
import { fetchDates } from "../../../../СalendarDates/model/services/fetchDates/fetchDates";

export const sendDate = createAsyncThunk<Date, void, ThunkConfig<string>>(
  "calendarControlPanel/sendDate",

  async (_, thunkApi) => {
    const { extra, dispatch, rejectWithValue, getState } = thunkApi;

    const userId = getUserId(getState());
    const date = getDateData(getState());
    const datesTotal = getCalendarDates.selectTotal;

    if (!userId || !date) {
      return rejectWithValue("no data");
    }
    try {
      const response = await extra.api.post<Date>("/dates", {
        id: Number(datesTotal) + 1,
        userId,
        ...date,
      });

      if (!response.data) {
        throw new Error();
      }

      dispatch(fetchDates(userId));

      return response.data;
    } catch (e) {
      console.log(e);
      return rejectWithValue("error");
    }
  }
);
