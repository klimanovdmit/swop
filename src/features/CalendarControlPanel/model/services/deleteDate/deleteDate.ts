import { createAsyncThunk } from "@reduxjs/toolkit";
import { ThunkConfig } from "app/providers/StoreProvider/config/StateShema";
import { Date, getIdDate } from "entities/Date";
import { getUserId } from "entities/User/model/selectors/getUserId/getUserId";
import { fetchDates } from "../../../../СalendarDates/model/services/fetchDates/fetchDates";

export const deleteDate = createAsyncThunk<{}, void, ThunkConfig<string>>(
  "calendarControlPanel/deleteDate",

  async (_, thunkApi) => {
    const { extra, dispatch, rejectWithValue, getState } = thunkApi;

    const userId = getUserId(getState());
    const dateId = getIdDate(getState());

    try {
      const response = await extra.api.delete<Date>(`/dates/${dateId}`);

      if (!response.data) {
        throw new Error();
      }

      dispatch(fetchDates(userId));

      return response.data;
    } catch (e) {
      console.log(e);
      return rejectWithValue("error");
    }
  }
);
