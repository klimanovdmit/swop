import { Button, Modal } from "antd";
import { Suspense, useState } from "react";
import { classNames } from "shared/lib/classNames/classNames";
import { Loader } from "shared/ui/Loader/Loader";
import s from "./LoginModal.module.scss";
import { LoginFormAsync } from "../LoginForm/LoginForm.async";

interface LoginModalProps {
  className?: string;
}

export const LoginModal = ({ className }: LoginModalProps) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const isOpen = () => {
    setIsModalOpen(true);
  };

  const onClose = () => {
    setIsModalOpen(false);
  };

  return (
    <div className={classNames(s.LoginForm, {}, [className])}>
      <Button type="link" onClick={isOpen}>
        Войти
      </Button>
      {isModalOpen && (
        <Modal
          title="Авторизация"
          open={isModalOpen}
          onCancel={onClose}
          className={s.modal}
          footer={null}
        >
          <Suspense fallback={<Loader />}>
            <LoginFormAsync onSuccess={onClose} />
          </Suspense>
        </Modal>
      )}
    </div>
  );
};
