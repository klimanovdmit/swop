import { Alert, Button, Form, Input } from "antd";
import { useSelector } from "react-redux";
import { classNames } from "shared/lib/classNames/classNames";
import { memo, useCallback } from "react";
import {
  DynamicMiduleLoader,
  ReducersList,
} from "shared/lib/components/DynamicMiduleLoader/DynamicMiduleLoader";
import { useAppDispatch } from "shared/lib/hooks/UseAppDispatch/useAppDispatch";
import { loginActions, loginReducer } from "../../model/slice/loginSlice";
import s from "./LoginForm.module.scss";
import { loginByUserName } from "../../model/services/loginByUsername/loginByUsername";
import { getLoginUsername } from "../../model/selectors/getLoginState/getLoginUsername/getLoginUsername";
import { getLoginIsLoading } from "../../model/selectors/getLoginState/getLoginIsLoading/getLoginIsLoading";
import { getLoginError } from "../../model/selectors/getLoginState/getLoginError/getLoginError";
import { getLoginPassword } from "../../model/selectors/getLoginState/getLoginPassword/getLoginPassword";

export interface LoginFormProps {
  className?: string;
  onSuccess: () => void;
}

const initialReducers: ReducersList = {
  loginForm: loginReducer,
};

const LoginForm = memo(({ className, onSuccess }: LoginFormProps) => {
  const { Item } = Form;
  const dispatch = useAppDispatch();

  const username = useSelector(getLoginUsername);
  const password = useSelector(getLoginPassword);
  const error = useSelector(getLoginError);
  const isLoading = useSelector(getLoginIsLoading);

  const onFinish = useCallback(async () => {
    const result = await dispatch(loginByUserName({ username, password }));
    if (result.meta.requestStatus === "fulfilled") {
      onSuccess();
    }
  }, [onSuccess, dispatch, password, username]);

  const changeForm = useCallback(
    (_: any, formData: { username: string; password: string }) => {
      dispatch(loginActions.setUsername(formData.username));
      dispatch(loginActions.setPassword(formData.password));
    },
    [dispatch]
  );

  return (
    <DynamicMiduleLoader reducers={initialReducers} removeAfterUnmount>
      <div className={classNames(s.LoginForm, {}, [className])}>
        {error && (
          <div>
            <Alert message={error} type="error" />
          </div>
        )}
        <Form
          name="loginForm"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          style={{ maxWidth: 600 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onValuesChange={changeForm}
          autoComplete="off"
        >
          <Item
            label="Username"
            name="username"
            rules={[{ required: true, message: "Please input your username!" }]}
          >
            <Input />
          </Item>

          <Item
            label="Password"
            name="password"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input.Password />
          </Item>

          <Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button type="primary" htmlType="submit" loading={isLoading}>
              Войти
            </Button>
          </Item>
        </Form>
      </div>
    </DynamicMiduleLoader>
  );
});

export default LoginForm;
