import { CalendarOutlined, ProfileOutlined } from "@ant-design/icons";
import { RoutePath } from "shared/config/routeConfig/routeConfig";
import { ReactNode } from "react";

export interface SidebarItemType {
  path: string;
  text: string;
  Icon: ReactNode;
  authOnly?: boolean;
}

export const SidebarItemsList: SidebarItemType[] = [
  {
    path: RoutePath.calendar,
    text: "Календарь",
    Icon: <ProfileOutlined />,
    authOnly: true,
  },
  {
    path: RoutePath.profile,
    text: "Профиль",
    Icon: <CalendarOutlined />,
    authOnly: true,
  },
];
