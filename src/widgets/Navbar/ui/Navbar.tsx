import { classNames } from "shared/lib/classNames/classNames";
import { useDispatch, useSelector } from "react-redux";
import { getUserAuthData, userActions } from "entities/User";
import { Button } from "antd";
import { memo } from "react";
import s from "./Navbar.module.scss";
import { LoginModal } from "../../../features/AuthByUsername/ui/LoginModal/LoginModal";

interface NavbarProps {
  className?: string;
}

export const Navbar = memo(({ className }: NavbarProps) => {
  const dispatch = useDispatch();
  const authData = useSelector(getUserAuthData);

  const onLogout = () => dispatch(userActions.logout());

  if (authData) {
    return (
      <div className={classNames(s.Navbar, {}, [className])}>
        <div className={s.links}>
          <Button type="link" onClick={onLogout}>
            Выйти
          </Button>
        </div>
      </div>
    );
  }
  return (
    <div className={classNames(s.Navbar, {}, [className])}>
      <div className={s.links}>
        <LoginModal />
      </div>
    </div>
  );
});
