import { classNames } from "shared/lib/classNames/classNames";
import { Button } from "shared/ui/Button/Button";

interface LangSwitcherProps {
  className?: string;
}

export const LangSwitcher = ({ className }: LangSwitcherProps) => (
  <Button className={classNames("", {}, [className])}>Язык</Button>
);
