import FullCalendar from "@fullcalendar/react";
import { DateSelectArg, EventClickArg } from "@fullcalendar/core";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import ruLocale from "@fullcalendar/core/locales/ru";
import { classNames } from "shared/lib/classNames/classNames";
import { Loader } from "shared/ui/Loader/Loader";

import { useAppDispatch } from "shared/lib/hooks/UseAppDispatch/useAppDispatch";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { getCalendarControlPanelMode } from "features/CalendarControlPanel/model/selectors/getCalendarControlPanelState/getCalendarControlPanelMode/getCalendarControlPanelMode";
import {
  calendarControlPanelActions,
  getCalendarControlPanelIsLoading,
  Mode,
} from "features/CalendarControlPanel";
import { sendDate } from "features/CalendarControlPanel/model/services/sendDate/sendDate";
import { getCalendarDates } from "features/СalendarDates/model/slices/calendarDatesSlice";
import { getCalendarDatesIsLoading } from "features/СalendarDates/model/selectors/dates";
import { fetchDates } from "features/СalendarDates/model/services/fetchDates/fetchDates";
import { dateActions, getDateData } from "entities/Date";
import { deleteDate } from "features/CalendarControlPanel/model/services/deleteDate/deleteDate";
import { DateModal } from "entities/Date/ui/DateModal/DateModal";
import { getUserAuthData } from "entities/User";
import { StateSchema } from "app/providers/StoreProvider";
import { Button } from "antd";
import { CloseOutlined } from "@ant-design/icons";
import s from "./Calendar.module.scss";
import { EventDetails } from "../../../entities/Date/model/types/date";

interface CalendarProps {
  className?: string;
  userId: string;
}

export const Calendar = ({ className, userId }: CalendarProps) => {
  const dispatch = useAppDispatch();
  const dates = useSelector(getCalendarDates.selectAll);
  const dateData = (id: string) =>
    useSelector((state: StateSchema) => getCalendarDates.selectById(state, id));

  const datesIsLoading = useSelector(getCalendarDatesIsLoading);
  const mode = useSelector(getCalendarControlPanelMode);
  const date = useSelector(getDateData);
  const dateIsLoading = useSelector(getCalendarControlPanelIsLoading);
  const isAuth = useSelector(getUserAuthData);

  const selectDate = (date: DateSelectArg) => {
    const { startStr, endStr, allDay } = date;

    dispatch(
      dateActions.setDateData({
        start: startStr,
        end: endStr,
        allDay,
      })
    );
    if ((mode === Mode.NONE || !mode) && isAuth) {
      dispatch(dateActions.setIsModalOpen(true));
    }
  };

  const eventClick = (event: EventClickArg) => {
    const { event: e } = event;
    console.log(e, "e");

    dispatch(
      dateActions.setDateData({
        id: e.id,
        start: e.startStr,
        end: e.endStr,
        allDay: e.allDay,
        eventDetails: e.extendedProps.eventDetails as EventDetails,
      })
    );

    if (mode === Mode.NONE || !mode) dispatch(dateActions.setIsModalOpen(true));
  };

  useEffect(() => {
    dispatch(fetchDates(userId));
  }, [dispatch]);

  const closeEditMode = () =>
    dispatch(calendarControlPanelActions.setMode(Mode.NONE));

  useEffect(() => {
    if (date && mode === Mode.ADD_DATE) {
      dispatch(sendDate());
    }

    if (date && mode === Mode.DELETE_DATE) {
      dispatch(deleteDate());
    }
  }, [dispatch, mode, date]);

  // if (datesIsLoading) {
  //   return (
  //     <div className={classNames(s.Calendar, {}, [className])}>
  //       <Loader />
  //     </div>
  //   );
  // }

  return (
    <>
      {mode !== Mode.NONE && (
        <div className={s.editMode}>
          <Button
            type="default"
            shape="circle"
            icon={<CloseOutlined />}
            size="large"
            onClick={closeEditMode}
          />
        </div>
      )}
      <div className={classNames(s.Calendar, {}, [className])}>
        {datesIsLoading ? (
          <Loader />
        ) : (
          <FullCalendar
            plugins={[timeGridPlugin, interactionPlugin, dayGridPlugin]}
            locale={ruLocale}
            headerToolbar={{
              left: "prev,next,today",
              center: "title",
              right: "myCustomButton,dayGridMonth,timeGridWeek,timeGridDay",
            }}
            select={selectDate}
            selectable={!datesIsLoading || !dateIsLoading}
            initialEvents={dates}
            nowIndicator
            eventClick={eventClick}
            themeSystem="standart"
          />
        )}

        <DateModal />
      </div>
    </>
  );
};
