import { Spin } from "antd";
import { classNames } from "shared/lib/classNames/classNames";
import s from "./Loader.module.scss";

interface LoaderProps {
  className?: string;
  size?: "small" | "large" | "default" | undefined;
}

export const Loader = ({ className, size }: LoaderProps) => (
  <div className={classNames(s.Loader, {}, [className])}>
    <Spin size={size} />
  </div>
);
