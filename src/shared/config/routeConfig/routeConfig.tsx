import { AboutPage } from "pages/AboutPage";
import AuthPage from "pages/AuthPage/ui/AuthPage";
import { UserCalendarPage } from "pages/UserCalendarPage";
import { MainPage } from "pages/MainPage";
import { NotFoundPage } from "pages/NotFoundPage";
import { ProfilePage } from "pages/ProfilePage";
import { RouteProps } from "react-router-dom";
import { CalendarPage } from "pages/CalendarPage ";

export type AppRoutesProps = RouteProps & {
  authOnly?: boolean;
};

export enum Approutes {
  MAIN = "main",
  AUTH = "auth",
  ABOUT = "about",
  PROFILE = "profile",
  CALENDAR = "calendar",
  CALENDAR_BOOKING = "calendar_booking",
  // last
  NOT_FOUND = "not_found",
}

export const RoutePath: Record<Approutes, string> = {
  [Approutes.MAIN]: "/",
  [Approutes.AUTH]: "/auth",
  [Approutes.ABOUT]: "/about",
  [Approutes.PROFILE]: "/profile",
  [Approutes.CALENDAR]: "/calendar",
  [Approutes.CALENDAR_BOOKING]: "/calendar/",

  [Approutes.NOT_FOUND]: "*",
};

export const routeConfig: Record<Approutes, AppRoutesProps> = {
  [Approutes.MAIN]: {
    path: RoutePath.main,
    element: <MainPage />,
    authOnly: true,
  },
  [Approutes.AUTH]: {
    path: RoutePath.auth,
    element: <AuthPage />,
  },

  [Approutes.ABOUT]: { path: RoutePath.about, element: <AboutPage /> },
  [Approutes.PROFILE]: {
    path: RoutePath.profile,
    element: <ProfilePage />,
    authOnly: true,
  },
  [Approutes.CALENDAR]: {
    path: RoutePath.calendar,
    element: <UserCalendarPage />,
    authOnly: true,
  },
  [Approutes.CALENDAR_BOOKING]: {
    path: `${RoutePath.calendar_booking}:id`,
    element: <CalendarPage />,
  },

  [Approutes.NOT_FOUND]: {
    path: RoutePath.not_found,
    element: <NotFoundPage />,
  },
};
