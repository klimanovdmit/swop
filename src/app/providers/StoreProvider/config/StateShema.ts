import { AxiosInstance } from "axios";
import {
  AnyAction,
  CombinedState,
  EnhancedStore,
  Reducer,
  ReducersMapObject,
} from "@reduxjs/toolkit";
import { UserSchema } from "entities/User";
import { LoginSchema } from "features/AuthByUsername/model/types/loginSchema";
import { NavigateOptions, To } from "react-router-dom";
import { ProfileSchema } from "entities/Profile/model/types/profile";
import { DateSchema } from "entities/Date";
import { CalendarDatesSchema } from "../../../../features/СalendarDates/model/types/CalendarDatesSchema";
import { CalendarControlPanelSchema } from "../../../../features/CalendarControlPanel/model/types/CalendarDatesSchema";

export interface StateSchema {
  user: UserSchema;
  calendarDates: CalendarDatesSchema;
  date: DateSchema;

  // Асинхронные редюсеры
  loginForm?: LoginSchema;
  profile?: ProfileSchema;
  calendarControlPanel?: CalendarControlPanelSchema;
}

export type StateSchemaKey = keyof StateSchema;

export interface ReducerManager {
  getReducerMap: () => ReducersMapObject<StateSchema>;
  reduce: (state: StateSchema, action: AnyAction) => CombinedState<StateSchema>;
  add: (key: StateSchemaKey, reducer: Reducer) => void;
  remove: (key: StateSchemaKey) => void;
}

export interface ReduxStoreWithManager extends EnhancedStore<StateSchema> {
  reducerManager: ReducerManager;
}

export interface ThunkExtraArg {
  api: AxiosInstance;
  navigate?: (to: To, options?: NavigateOptions) => void;
}

export interface ThunkConfig<T> {
  rejectValue: T;
  extra: ThunkExtraArg;
  state: StateSchema;
}
