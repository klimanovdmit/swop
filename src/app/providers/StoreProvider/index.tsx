import type {
  StateSchema,
  ReduxStoreWithManager,
  ThunkConfig,
} from "./config/StateShema";
import { StoreProvider } from "./ui/StoreProvider";
import { createReduxStore } from "./config/store";
import type { AppDispatch } from "./config/store";

export {
  StoreProvider,
  createReduxStore,
  StateSchema,
  ReduxStoreWithManager,
  AppDispatch,
  ThunkConfig,
};
