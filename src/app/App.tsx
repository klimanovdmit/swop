import "./styles/index.scss";
import "antd/dist/reset.css";
import { classNames } from "shared/lib/classNames/classNames";
import { Navbar } from "widgets/Navbar";
import { Sidebar } from "widgets/Sidebar";
import { Suspense, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getUserInited, userActions } from "entities/User";
import { ConfigProvider } from "antd";
import { AppRouter } from "./providers/router";
import { useTheme } from "./providers/ThemeProvider";
import { customTheme } from "./styles/themes/customTheme";

export function App() {
  const { theme } = useTheme();
  const dispatch = useDispatch();

  const inited = useSelector(getUserInited);

  useEffect(() => {
    dispatch(userActions.initAuthData());
  }, [dispatch]);

  return (
    <ConfigProvider theme={customTheme}>
      <div className={classNames("app", {}, [theme])}>
        <Suspense fallback="">
          <Navbar />
          <div className="content-page">
            <Sidebar />
            {inited && <AppRouter />}
          </div>
        </Suspense>
      </div>
    </ConfigProvider>
  );
}
