import { classNames } from "shared/lib/classNames/classNames";
import { Alert, Button, Form, Input, Space } from "antd";
import { Loader } from "shared/ui/Loader/Loader";
import { useCallback } from "react";
import { updateProfileData } from "../../model/services/updateProfileData/updateProfileData";
import { Profile } from "../../model/types/profile";
import s from "./ProfileCard.module.scss";
import { useAppDispatch } from "../../../../shared/lib/hooks/UseAppDispatch/useAppDispatch";

interface ProfileCardProps {
  className?: string;
  data?: Profile;
  isLoading?: boolean;
  error?: string;
  readonly?: boolean;
  onChangeProfileData: (value?: Profile) => void;
}

export const ProfileCard = ({
  className,
  data,
  isLoading,
  error,
  readonly,
  onChangeProfileData,
}: ProfileCardProps) => {
  const dispatch = useAppDispatch();
  const { Item } = Form;

  const onSave = useCallback(() => {
    dispatch(updateProfileData());
  }, []);

  if (isLoading) {
    return (
      <div className={classNames(s.ProfileCard, {}, [className])}>
        <Loader />
      </div>
    );
  }

  if (error) {
    return (
      <div className={classNames(s.ProfileCard, {}, [className])}>
        <Alert
          message="Произошла ошибка при загрузке профиля"
          description="Попробуйте перезагрузить страницу"
          type="error"
        />
      </div>
    );
  }

  return (
    <div className={classNames(s.ProfileCard, {}, [className])}>
      <div className={s.data}>
        <Space direction="vertical" align="center" wrap size={4}>
          {data?.avatar && <img src={data?.avatar} alt="avatr" />}
        </Space>
        <Form
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          name="profile"
          initialValues={data}
          onValuesChange={onChangeProfileData}
          onFinish={onSave}
          style={{ maxWidth: 600 }}
          disabled={readonly}
        >
          <Item
            name="username"
            label="Имя пользователя"
            rules={[{ required: true }]}
          >
            <Input />
          </Item>
          <Item name="firstname" label="Имя" rules={[{ required: true }]}>
            <Input placeholder="Ваше имя" />
          </Item>
          <Item name="lastname" label="Фамилия" rules={[{ required: true }]}>
            <Input placeholder="Ваша фамилия" />
          </Item>
          <Item wrapperCol={{ span: 16, offset: 8 }}>
            <Button type="primary" htmlType="submit">
              Сохранить
            </Button>
          </Item>
        </Form>
      </div>
    </div>
  );
};
