export enum RoleType {
  ADMIN = "admin",
  USER = "user",
}

export interface User {
  id: string;
  username: string;
  role: RoleType;
}

export interface UserSchema {
  authData?: User;
  _inited: boolean;
}
