import { classNames } from "shared/lib/classNames/classNames";
import { Button, Form, Input, Space } from "antd";
import { getUserAuthData } from "entities/User";
import { useSelector } from "react-redux";
import { EventDetails } from "entities/Date/model/types/date";
import { Suspense } from "react";
import { Loader } from "shared/ui/Loader/Loader";
import { useAppDispatch } from "shared/lib/hooks/UseAppDispatch/useAppDispatch";
import { CalendarControlPanel } from "features/CalendarControlPanel/ui/CalendarConrolPanel";
import {
  Mode,
  calendarControlPanelActions,
} from "features/CalendarControlPanel";
import { dateActions } from "entities/Date/model/slice/DateSlice";
import { deleteDate } from "../../../../features/CalendarControlPanel/model/services/deleteDate/deleteDate";
import s from "./DateForm.module.scss";

interface DateFormProps {
  className?: string;
  initValues?: EventDetails;
}

export const DateForm = ({ className, initValues }: DateFormProps) => {
  const dispatch = useAppDispatch();

  const { Item } = Form;
  const isAuth = useSelector(getUserAuthData);

  const deleteDate = () => {
    dispatch(calendarControlPanelActions.setMode(Mode.DELETE_DATE));
    dispatch(dateActions.setIsModalOpen(false));
    // dispatch(calendarControlPanelActions.setMode(Mode.NONE));
  };

  return (
    <div className={classNames(s.DateForm, {}, [className])}>
      <Suspense fallback={<Loader />}>
        <Form
          name="dateForm"
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 16 }}
          style={{ maxWidth: 600 }}
          initialValues={initValues}
          autoComplete="off"
        >
          <Item label="Имя" name="name">
            <Input />
          </Item>
          <Item label="Номер телефона" name="phone">
            <Input />
          </Item>
          <Space>
            <Button type="primary">
              {isAuth ? "Сохранить" : "Записаться"}
            </Button>
            {isAuth && (
              <Button type="default" onClick={deleteDate}>
                Удалить
              </Button>
            )}
          </Space>
        </Form>
      </Suspense>
    </div>
  );
};
