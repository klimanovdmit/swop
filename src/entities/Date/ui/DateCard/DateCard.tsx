import { classNames } from "shared/lib/classNames/classNames";
import { Card } from "antd";
import { CalendarOutlined, ClockCircleOutlined } from "@ant-design/icons";
import dayjs from "dayjs";
import s from "./DateCard.module.scss";

interface DateCardProps {
  className?: string;
  start?: string;
  end?: string;
}

export const DateCard = ({ className, start, end }: DateCardProps) => (
  <div className={classNames(s.DateCard, {}, [className])}>
    <Card style={{ width: 300 }}>
      <p>
        <CalendarOutlined />
        <span>{dayjs(start).format("DD MMMM")}</span>
      </p>
      <p>
        <ClockCircleOutlined />
        <span>
          {`${dayjs(start).format("HH:MM")}-${dayjs(end).format("HH:MM")}`}
        </span>
      </p>
    </Card>
  </div>
);
