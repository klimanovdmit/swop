import { Modal } from "antd";
import { Suspense } from "react";
import { classNames } from "shared/lib/classNames/classNames";
import { Loader } from "shared/ui/Loader/Loader";
import { useSelector } from "react-redux";
import { useAppDispatch } from "shared/lib/hooks/UseAppDispatch/useAppDispatch";
import { dateActions } from "entities/Date/model/slice/DateSlice";
import { getIsModalOpen } from "entities/Date/model/selectors/getIsModalOpen/getIsModalOpen";
import { getDateData } from "entities/Date/model/selectors/getDateData/getDateData";
import {
  Mode,
  calendarControlPanelActions,
} from "features/CalendarControlPanel";
import { DateCard } from "../DateCard/DateCard";
import { DateForm } from "../DateForm/DateForm";
import s from "./DateModal.module.scss";

interface DateModalProps {
  className?: string;
}

export const DateModal = ({ className }: DateModalProps) => {
  const dispatch = useAppDispatch();

  const isModalOpen = useSelector(getIsModalOpen);
  const dateData = useSelector(getDateData);

  const onClose = () => {
    dispatch(dateActions.setIsModalOpen(false));

    dispatch(calendarControlPanelActions.setMode(Mode.NONE));
  };
  console.log(dateData, "date");
  return (
    <div className={classNames(s.DateModal, {}, [className])}>
      {isModalOpen && (
        <Modal open={isModalOpen} onCancel={onClose} footer={null}>
          <Suspense fallback={<Loader />}>
            <div className={s.modalContent}>
              <DateCard start={dateData?.start} end={dateData?.start} />
              <DateForm initValues={dateData?.eventDetails} />
            </div>
          </Suspense>
        </Modal>
      )}
    </div>
  );
};
