import { StateSchema } from "app/providers/StoreProvider";

export const getDateData = (state: StateSchema) => state.date.dateData;
