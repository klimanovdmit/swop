import { StateSchema } from "app/providers/StoreProvider";

export const getIdDate = (state: StateSchema) => state.date.dateData?.id;
