import { StateSchema } from "app/providers/StoreProvider";

export const getIsModalOpen = (state: StateSchema) => state.date.isModalOpen;
