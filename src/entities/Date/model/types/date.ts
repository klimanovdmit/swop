export interface EventDetails {
  name: string;
  phone: string;
}

export interface Date {
  id: string;
  userId: number;
  title?: string;
  start: string;
  end: string;
  allDay: boolean;
  eventDetails?: EventDetails;
  backgroundColor?: string;
}

export interface CalendarDateData {
  id?: string;
  userId?: number;
  title?: string;
  start?: string;
  end?: string;
  allDay?: boolean;
  eventDetails?: EventDetails;
  backgroundColor?: string;
}

export interface DateSchema {
  dateData?: CalendarDateData;
  isModalOpen: boolean;
}
