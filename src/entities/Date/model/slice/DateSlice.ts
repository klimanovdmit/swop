import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { CalendarDateData, DateSchema } from "../types/date";

const initialState: DateSchema = { isModalOpen: false };

export const userSlice = createSlice({
  name: "date",
  initialState,
  reducers: {
    setDateData: (state, action: PayloadAction<CalendarDateData>) => {
      state.dateData = action.payload;
    },

    setIsModalOpen: (state, action: PayloadAction<boolean>) => {
      state.isModalOpen = action.payload;
    },
  },
});

export const { actions: dateActions } = userSlice;
export const { reducer: dateReducer } = userSlice;
